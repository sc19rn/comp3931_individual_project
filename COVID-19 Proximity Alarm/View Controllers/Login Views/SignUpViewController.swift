//
//  SignUpViewController.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 13/03/2022.
//

import UIKit
import FirebaseAuth
import Firebase

class SignUpViewController: UIViewController {

    
    @IBOutlet weak var FirstNameTextField: UITextField!
    @IBOutlet weak var LastNameTextField: UITextField!
    
    @IBOutlet weak var DateOfBirthTextField: UITextField!
    private var datePicker: UIDatePicker?
    
    @IBOutlet weak var HomeAddressTextField: UITextField!
    
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    
    @IBOutlet weak var SignUpButton: UIButton!
    
    @IBOutlet weak var ErrorWarning: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(SignUpViewController.dateChanged(datePicker:)), for: .valueChanged)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        DateOfBirthTextField.inputView = datePicker
        
        // Do any additional setup after loading the view.
        setUpElements()
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    @objc func dateChanged(datePicker: UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        DateOfBirthTextField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    func setUpElements()
    {
        ErrorWarning.alpha = 0
    }
    
    func validateFields() -> String?{
        //Check all fields are filled in
        if FirstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || LastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || DateOfBirthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || HomeAddressTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || EmailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || PasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            return "Please fill in all fields."
        }
        
        //Check if the password is secure
        let cleanedPassword = PasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if Utilities.isPasswordValid(cleanedPassword) == false {
            return "Please make sure your password is at least 8 characters, contains a special character and a number"
        }
        
        return nil
    }

    @IBAction func SignUpTapped(_ sender: Any) {
        //Validate the fields
        let error = validateFields()
        
        if error != nil {
            showError(error!)
        }
        else{
            //Create cleaned version of data
            let firstname = FirstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastname = LastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let dateofbirth = DateOfBirthTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let homeaddress = HomeAddressTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = EmailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = PasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            //Create an user
            Auth.auth().createUser(withEmail: email, password: password)
            {
                (result, err) in
                //Check for errors
                if err != nil {
                    self.showError("Error creating user")
                }
                else{
                    //If user is created successfully, store first name and last name
                    let db = Firestore.firestore()
                    
                    db.collection("users").addDocument(data: ["firstname":firstname, "lastname":lastname, "date of birth": dateofbirth, "home address": homeaddress, "uid":result!.user.uid]){
                        (error) in
                        if error != nil {
                            //Show error message
                            self.showError("Error saving data")
                        }
                    }
                    self.performSegue(withIdentifier: "activateBluetooth", sender: self)
                    self.performSegue(withIdentifier: "loginHomePage", sender: self)
                }
            }
        }
    }
    
    func showError(_ message:String){
        
        ErrorWarning.text = message
        ErrorWarning.alpha = 1
    }
}
