//
//  SignInViewController.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 13/03/2022.
//

import UIKit
import FirebaseAuth

class SignInViewController: UIViewController {

    
    @IBOutlet weak var EmailTextField: UITextField!
    
    @IBOutlet weak var PassWordTextField: UITextField!
    
    @IBOutlet weak var SignInTextField: UIButton!
    
    @IBOutlet weak var ErrorWarning: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpElements()
    }
    
    func setUpElements() {
        ErrorWarning.alpha = 0
    }
    
    @IBAction func SignInTapped(_ sender: Any) {
        
        //Create cleaned version of the text field
        let email = EmailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = PassWordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        //Signing in the user
        Auth.auth().signIn(withEmail: email, password: password){
            (result, error) in
            if error != nil {
                //Couldn't sign in
                self.ErrorWarning.text = error!.localizedDescription
                self.ErrorWarning.alpha = 1
            }
            else{
                self.performSegue(withIdentifier: "activateBluetooth", sender: self)
                self.performSegue(withIdentifier: "loginHomePage", sender: self)
            }
        }
    }
}
