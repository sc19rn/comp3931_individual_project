//
//  NoUsersTableViewCell.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 04/04/2022.
//

import UIKit

//Shown when there are no other devices detected in the last detecting.
class NoDeviceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btSignalImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btSignalImageView.image = btSignalImageView.image?.withRenderingMode(.alwaysTemplate)
        btSignalImageView.tintColor = UIColor.green
    }
}
