//
//  DeviceTableViewController.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 14/03/2022.
//

import UIKit

class DeviceTableViewController: UITableViewController,  DeviceRepositoryListener {
    
    //List of detected devices
    var deviceArray = [Device]()
    
    //MARK: Show a list of currently detected devices
    //Table View
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Detected Devices"
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.register(DetectedDevicesTableViewCell.classForCoder(), forCellReuseIdentifier: "DeviceItemCell")
    }
    
    // If the view is getting loaded
    // Update to the Device Repository
    override func viewWillAppear(_ animated: Bool) {
        DeviceRepository.sharedInstance.currentListener = self
        onRepositoryUpdate()
    }

    //##############################################################################################
    //Set tableView
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceArray.count > 0 ? deviceArray.count : 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (deviceArray.count > 0) {
            return configureDeviceItemCell(indexPath: indexPath)
        } else {
            return configureNoDeviceCell(indexPath: indexPath)
        }
    }
    //##############################################################################################
    
    //If the deviceArray has any element, then the table will display them.
    //MARK: Set each cell to represent a device from the last detection
    //
    private func configureDeviceItemCell(indexPath: IndexPath) -> UITableViewCell {
        //Setting a cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceItemCell", for: indexPath) as! DetectedDevicesTableViewCell
        
        //Access element of the deviceArray for the cell
        let device = deviceArray[indexPath.row]
        
        //Extract the signal strength information
        let power = Utilities.signlaStrength(rssi: device.rssi, txPower: device.txPower)
        
        //Classify the type of warning
        let classification = Utilities.classifySignalStrength(power)
        
        //Add the signal strength and warning to the cell
        cell.signalClassification = classification
        cell.signalStrength = power
        
        return cell
    }

    //If the deviceArray is empty,
    //MARK: Set a cell that displays no user is detected
    //
    private func configureNoDeviceCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoDeviceItemCell", for: indexPath) as! NoDeviceTableViewCell
        return cell
    }
    
    // If the repository updates new devices
    // Present them
    func onRepositoryUpdate() {
        deviceArray = DeviceRepository.sharedInstance.getCurrentDevices()
        tableView.reloadData()
    }
    
    @IBAction func activateBluetooth( _ seg: UIStoryboardSegue) {
        let bluetoothManager = CoreBluetoothManager.sharedInstance
        bluetoothManager.delegate = (DeviceRepository.sharedInstance as BluetoothManagerDelegate)
        CoreBluetoothManager.sharedInstance.startScanning()
        CoreBluetoothManager.sharedInstance.startAdvertising()
    }
}


