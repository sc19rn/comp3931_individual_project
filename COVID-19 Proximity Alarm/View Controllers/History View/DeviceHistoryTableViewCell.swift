//
//  DeviceHistoryTableViewCell.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 05/04/2022.
//

import UIKit

class DeviceHistoryTableViewCell: UITableViewCell {

    // MARK: Properties
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var personIcon: UIImageView!
    @IBOutlet weak var distanceDescription: UILabel!
    @IBOutlet weak var signalStrength: UILabel!
    @IBOutlet weak var bluetoothIcon: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
