//
//  HistoryTableViewController.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 05/04/2022.
//

import UIKit

class HistoryTableViewController: UITableViewController, DeviceRepositoryListener {
    //Format the date of detection
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    //List of detected devices
    var deviceArray = [Device]()

    //Present the Viewtable
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.register(DetectedDevicesTableViewCell.classForCoder(), forCellReuseIdentifier: "HistoryItemCell")
    }
    
    // If the view is getting loaded
    // Update to the Device Repository
    override func viewWillAppear(_ animated: Bool)
    {
        DeviceRepository.sharedInstance.currentListener = self
        onRepositoryUpdate()
    }

    //Set tableView
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryItemCell", for: indexPath) as! DetectedDevicesTableViewCell
          
        let device = deviceArray[indexPath.row]
        
        let power = Utilities.signlaStrength(rssi: device.rssi, txPower: device.txPower)
        let classification = Utilities.classifySignalStrength(power)
        
        cell.signalClassification = classification
        cell.signalStrength = power
        //cell.isSafeUser = device.isSafeUser
        
        if cell.extraViewOnRightSide == nil {
            let dateLabel = UILabel()
            dateLabel.textColor = .gray
            dateLabel.font = .systemFont(ofSize: 14.0, weight: .medium)
            cell.extraViewOnRightSide = dateLabel
        }
        
        let dateLabel = cell.extraViewOnRightSide as? UILabel
        dateLabel?.text = dateFormatter.string(from: device.scanDate!)
        
        
        return cell
    }

    //Update to the device repository
    func onRepositoryUpdate()
    {
        deviceArray = DeviceRepository.sharedInstance.getAllDevices()
        tableView.reloadData()
    }
}

