//
//  Utilities.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 14/03/2022.
//

import Foundation
import UIKit

public enum SignalClassification {
    case danger
    case close
    case warning
    case ok
}

public struct Utilities {
    static let signalDistanceOk = 53 // This or lower is socially distant = green
    static let signlaDistanceLightWarn = 56 // This to SIGNAL_DISTANCE_OK warning = yellow
    static let signalDistanceStrongWarn = 63 // This to SIGNAL_DISTANCE_LIGHT_WARN strong warning = orange
    
    public static func signlaStrength(rssi: Int32, txPower: Int32) -> Int32 {
        let signal = SignalConstants.assumedTxPower + rssi
        return signal
    }
    
    public static func classifySignalStrength(_ power: Int32) -> SignalClassification {
        if (power > Utilities.signalDistanceStrongWarn) {
            return .danger
        } else if (power > Utilities.signlaDistanceLightWarn) {
            return .close
        } else if (power > Utilities.signalDistanceOk) {
            return .warning
        } else {
            return .ok
        }
    }
    
    public static func styleTextField(_ textfield:UITextField) {
        
        // Create the bottom line
        let bottomLine = CALayer()
        
        bottomLine.frame = CGRect(x: 0, y: textfield.frame.height - 2, width: textfield.frame.width, height: 2)
        
        bottomLine.backgroundColor = UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1).cgColor
        
        // Remove border on text field
        textfield.borderStyle = .none
        
        // Add the line to the text field
        textfield.layer.addSublayer(bottomLine)
        
    }
    
    public static func styleFilledButton(_ button:UIButton) {
        
        // Filled rounded corner style
        button.backgroundColor = UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1)
        button.layer.cornerRadius = 25.0
        button.tintColor = UIColor.white
    }
    
    public static func styleHollowButton(_ button:UIButton) {
        
        // Hollow rounded corner style
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.cornerRadius = 25.0
        button.tintColor = UIColor.black
    }
    
    public static func isPasswordValid(_ password : String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
}
