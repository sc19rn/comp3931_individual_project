//
//  Comstants.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 14/03/2022.
//

import Foundation

enum SignalConstants {
    static let traceInterval: Double = 10
    static let signalDistanceOk = 53 // This or lower is socially distant = green
    static let signlaDistanceLightWarn = 56 // This to SIGNAL_DISTANCE_OK warning = yellow
    static let signalDistanceStrongWarn = 63 // This to SIGNAL_DISTANCE_LIGHT_WARN strong warning = orange
    // ...and above this is not socially distant = red
    
    static let assumedTxPower: Int32 = 127
    static let iosSignalReduction: Int32 = 17
}



