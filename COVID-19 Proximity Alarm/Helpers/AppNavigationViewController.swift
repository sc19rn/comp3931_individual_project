//
//  AppNavigationViewController.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 04/04/2022.
//

import UIKit

/// Standard app navigation
class AppNavigationViewController: UINavigationController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = .theme
            navigationBar.standardAppearance = navBarAppearance
            navigationBar.scrollEdgeAppearance = navBarAppearance
        }
    }
}

extension UIViewController {
    func addSettingsIcon() {
        let icon = UIImage(named: "settingsIcon")?.withRenderingMode(.alwaysOriginal)
        let settingsButton = UIBarButtonItem(image: icon, landscapeImagePhone: icon, style: .plain, target: self, action: #selector(moveToSettings))
        navigationItem.rightBarButtonItem = settingsButton
    }
    
    @objc func moveToSettings() {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
    }
}
