//
//  theme.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 04/04/2022.
//

import UIKit

extension UIColor {
    static let theme = UIColor(named: "theme")
}
