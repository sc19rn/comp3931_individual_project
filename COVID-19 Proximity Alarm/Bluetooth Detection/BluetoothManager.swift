//
//  BluetoothManager.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 14/03/2022.
//

import Foundation
import CoreBluetooth
import AudioToolbox

//Set Bluetooth Signal Constants
public enum BSConstants: String {
    case DEVICE_SERVICE_UUID = "00086f9a-264e-3ac6-838a-000000000000"
    case DEFAULTS_UUID_KEY = "Device UUID"
}

//Take prefix of the device's UUID
public extension CBUUID {
    
    static let devicePrefix = String(BSConstants.DEVICE_SERVICE_UUID.rawValue.prefix(7))
    
    var hasDevicePrefix: Bool {
        let lowercased = uuidString.lowercased()
        return lowercased.hasPrefix(CBUUID.devicePrefix)
    }
}

@objcMembers
public class DiscoverdDevice: NSObject {
    public let uuid: String
    public var rssi: NSNumber
    public var txPower: NSNumber?
    
    //Initialise device's infor
    init(CBUUID: CBUUID, rssi: NSNumber, txPower: NSNumber?) {
        self.uuid = CBUUID.uuidString
        self.rssi = rssi
        self.txPower = txPower
    }
}

//Set protocol for methods##########################################
@objc
public protocol BluetoothManagerDelegate: AnyObject {
    func peripheralsDidUpdate()
    func advertisingStarted()
    func scanningStarted()
    func didDiscoverPeripheral(ids: [DiscoverdDevice])
}
//##################################################################
public protocol BluetoothManager {
    var peripherals: Dictionary<UUID, CBPeripheral> { get }
    var delegate: BluetoothManagerDelegate? { get set }
    var uuidString: String { get set }
    func pause(_ with: Bool)
    func startAdvertising()
    func startScanning()
}
//##################################################################

//Generate UUID
func getNewUniqueId() -> String {
    let stringChars = "0123456789abcdef"
    let postfix = String((0...11).map{ _ in stringChars.randomElement()! })
    return BSConstants.DEVICE_SERVICE_UUID.rawValue.replacingOccurrences(of: "000000000000", with: postfix)
}


@objcMembers
public class CoreBluetoothManager: NSObject, BluetoothManager {
    
    //Collect the UUID
    //Or generate a new one if the current UUID is not existed
    public var uuidString: String =  {
        let currentId = UserDefaults.standard.string(forKey: BSConstants.DEFAULTS_UUID_KEY.rawValue)
        if (currentId == nil) {
            let newId = getNewUniqueId()
            UserDefaults.standard.set(newId, forKey: BSConstants.DEFAULTS_UUID_KEY.rawValue)
            return newId
        } else {
            return currentId!
        }
    }()
    
    //Reset the UUID
    public func resetUuidString(){
        let newId = getNewUniqueId()
        uuidString = newId
        UserDefaults.standard.set(newId, forKey: BSConstants.DEFAULTS_UUID_KEY.rawValue)
    }
    
    let devicePrefix = String(BSConstants.DEVICE_SERVICE_UUID.rawValue.prefix(7))
    
    public static let sharedInstance: CoreBluetoothManager = {
        let instance = CoreBluetoothManager()
        return instance
    }()
    
    //Pause scanning and broadcasting and let the user know with a series of 5 vibrations
    public func pause(_ with: Bool) {
        self.isPaused = with
        
        if (isPaused) {
            // The series of 5 vibrations
            AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) {
                AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) {
                    AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) {
                        AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) {
                            AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) {}
                        }
                    }
                }
            }
            print("Pausing...")
            peripheralManager?.stopAdvertising()
            peripheralManager?.delegate = nil
            centralManager?.delegate = nil
            centralManager?.stopScan()
        }
    }
    
    public var isPaused: Bool = true
    weak public var delegate: BluetoothManagerDelegate?
    //Setting list of peripherals
    private(set) public var peripherals = Dictionary<UUID, CBPeripheral>() {
        didSet {
            delegate?.peripheralsDidUpdate()
        }
    }
    //Start BLE advertising its presence
    public func startAdvertising() {
        isPaused = false
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil,
                                                options: nil)
    }
    
    //Start BLE scanning for other devices with the app running on them
    public func startScanning() {
        isPaused = false
        centralManager = CBCentralManager(delegate: self, queue: nil,
            options: nil)

    }

    //MARK: Private properties
    private var peripheralManager: CBPeripheralManager?
    private var centralManager: CBCentralManager?
    private var name: String?
    private var discoveredDevices: Dictionary<CBUUID, DiscoverdDevice> = [:]
}

//MARK: Peripheral Manager
extension CoreBluetoothManager: CBPeripheralManagerDelegate {
    
    //Called when the state of peripheral manager changes
    //Set up advertising
    public func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if peripheral.state == .poweredOn {
            if peripheral.isAdvertising {
                peripheral.stopAdvertising()
            }

            let uuid = CBUUID(string: uuidString)
            var advertisingData: [String : Any] = [
                CBAdvertisementDataLocalNameKey: "covid19-proximity-alarm",
                CBAdvertisementDataServiceUUIDsKey: [uuid]
            ]
                advertisingData[CBAdvertisementDataLocalNameKey] = name
            self.broadcastToDevices(peripheralManager: peripheralManager!, advertisingData: advertisingData)
            
        } else {
            #warning("handle other states")
        }
    }
    
    // Start broadcasting
    // Use a timer to stop the broadcast if it exceeds the period of time. For saving energy goal.
    func broadcastToDevices(peripheralManager: CBPeripheralManager, advertisingData: [String : Any]) {
        // Default to the packaged repository if no device is detected.
        if (delegate == nil) { delegate = DeviceRepository.sharedInstance }
        
        if (peripheralManager.isAdvertising) {
            peripheralManager.stopAdvertising()
        }

        if (!isPaused) {
            let uuid = CBUUID(string: uuidString)
            var advertisingData: [String : Any] = [
                CBAdvertisementDataLocalNameKey: "covid19-proximity-alarm",
                CBAdvertisementDataServiceUUIDsKey: [uuid]
            ]

            advertisingData[CBAdvertisementDataLocalNameKey] = "SDAlarm"
        
            delegate?.advertisingStarted()
            peripheralManager.startAdvertising(advertisingData)

            Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { [weak self] _ in
                self?.broadcastToDevices(peripheralManager: peripheralManager, advertisingData: advertisingData)
            }
        }
    }
}

//MARK: Central manager
extension CoreBluetoothManager: CBCentralManagerDelegate {
    
    //Called when the state of the central manager changes
    //Set up scanning
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            detectForDevices(central: central)
        } else {
            #warning("Error handling")
        }
    }
    
    //Set up scaning for other apps
    func detectForDevices(central: CBCentralManager) {
        // Default to the packaged repository if no device is detected.
        if (delegate == nil) { delegate = DeviceRepository.sharedInstance }

        if central.isScanning {
            central.stopScan()
            
            // Deliver discovered devices
            delegate?.didDiscoverPeripheral(ids: Array(discoveredDevices.values))
        }

        if (!isPaused) {
            discoveredDevices = [:]
            delegate?.scanningStarted()
            central.scanForPeripherals(withServices: nil)

            Timer.scheduledTimer(withTimeInterval: SignalConstants.traceInterval, repeats: false) { [weak self] _ in
                self?.detectForDevices(central: central)
            }
        }
    }
    
    //MARK: Called when there are devices detected
    //Filter the devices for ones that start with the app's UUID.
    //If a device was previously detected we use a rolling average over the scan period to calcualte the signal strength for the device.
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String: Any], rssi RSSI: NSNumber) {
        peripherals[peripheral.identifier] = peripheral
        let uuids = advertisementData[CBAdvertisementDataServiceUUIDsKey] as? [CBUUID]
        let txPowerLevel = advertisementData[CBAdvertisementDataTxPowerLevelKey] as? NSNumber
        
        guard let ids = uuids, !ids.filter({ (cbUid) -> Bool in
            return cbUid.hasDevicePrefix
        }).isEmpty else { return }
        
        // Check to see if the device was already present and calculate a rolling average...
        for id in ids {
            let currentDevice = discoveredDevices[id]
            if (currentDevice == nil) {
                discoveredDevices[id] = DiscoverdDevice(CBUUID: id, rssi: RSSI, txPower: txPowerLevel)
            }
            else {
                discoveredDevices[id]?.rssi = NSNumber(value: (currentDevice!.rssi.intValue + RSSI.intValue) / 2)
            }
        }
    }
}



