//
//  DeviceRepository.swift
//  COVID-19 Proximity Alarm
//
//  Created by Roman Nguyen on 15/03/2022.
//

import UIKit
import CoreData
import AudioToolbox

public protocol DeviceRepositoryListener {
    func onRepositoryUpdate()
}

class PersistentContainer: NSPersistentContainer {}

public class DeviceRepository {
    
    public var currentListener: DeviceRepositoryListener? = nil
    
    //This property is optional since there are legitimate error conditions that could cause the creation of the storefor the application to fail.
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = PersistentContainer(name: "Covid19_Proximity_Alarm")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                /*
                 Some errors like:
                 * The parent directory does not exist.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    public static let sharedInstance: DeviceRepository = {
        let instance = DeviceRepository()
        return instance
    }()
    
    //MARK: Add a new device to the database
    // - Information of each device:
    //   - deviceUuid: The UUID
    //   - rssi: The signal strength
    //   - txPower: The transmission power
    //   - scanDate: The date of detection
    func insert(deviceUuid: String, rssi: Int32, txPower: Int32?, scanDate: Date) {
        // Add the device to the database
        let newDevice = Device(context: self.context)
        newDevice.deviceUuid = deviceUuid
        newDevice.rssi = rssi
        if (txPower != nil) {
            newDevice.txPower = txPower!
        }
        newDevice.scanDate = scanDate
        //newDevice.isSafeUser = isSafeUser(uuidString: deviceUuid)
    }
    
    //Alert the update
    func updateCurrentDevices() {
        do {
            try context.save()
        } catch {
            print("Error saving context \(error)")
        }
        
        currentListener?.onRepositoryUpdate()
        doAlerts()
    }
    
    //MARK: Alert the user to the proximity of other users
    //Play sound with a series of alarms
    func doAlerts() {
        var tooClose = false
        var danger = false
        var warn = false
        for device in getCurrentDevices() {
            //if (!device.isSafeUser) {
                let signal = Utilities.signlaStrength(rssi: device.rssi, txPower: device.txPower)
                
                if (signal >= SignalConstants.signalDistanceStrongWarn) {
                    tooClose = true
                } else if (signal >= SignalConstants.signlaDistanceLightWarn) {
                    danger = true
                } else if (signal >= SignalConstants.signalDistanceOk) {
                    warn = true
                }
            //}
        }
        
        //Play sound with each level of warning.
        if (tooClose) {
            AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) {
                AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) {
                    AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }
                }
            }
        } else if (danger) {
            AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) {
                AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }
            }
        } else if (warn) {
            AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(1520)) { }
        }
    }
    
    //MARK: Get the devices from the last scan
    //Returns: The devices detected during the last scan interval
    public func getCurrentDevices() -> [Device] {
        var deviceArray = [Device]()
        let startTime = (Date() - SignalConstants.traceInterval * 4) as NSDate
        let timePredicate = NSPredicate(format: "scanDate >= %@", startTime)
        let request: NSFetchRequest<Device> = Device.fetchRequest()
        request.predicate = timePredicate
        let sortDesc = NSSortDescriptor(key: "rssi", ascending: false)
        request.sortDescriptors = [sortDesc]
        
        do {
            deviceArray = try context.fetch(request)
        } catch {
            print("Error fetching current devices \(error)")
        }
        
        //Remove duplicate scans and average the values
        var averagedDevices = [Device]()
        for device in deviceArray {
            let current = averagedDevices.first(where: {$0.deviceUuid?.lowercased() == device.deviceUuid?.lowercased()})
            if (current != nil) {
                current!.rssi = (current!.rssi + device.rssi) / 2
                current!.txPower = (current!.txPower + device.txPower) / 2
            } else {
                averagedDevices.append(device)
            }
        }
        
        return averagedDevices
    }
    
    //MARK: Get all the devices that have been detected
    //Returns: The array of all devices that have been detected
    public func getAllDevices() -> [Device] {
        var deviceArray = [Device]()
        let request: NSFetchRequest<Device> = Device.fetchRequest()
        let sortDesc = NSSortDescriptor(key: "scanDate", ascending: false)
        request.sortDescriptors = [sortDesc]
        
        do {
            deviceArray = try context.fetch(request)
        } catch {
            print("Error fetching all devices \(error)")
        }
        
        return deviceArray
    }
}

extension DeviceRepository: BluetoothManagerDelegate {
    public func peripheralsDidUpdate() {}
    
    public func advertisingStarted() {}
    
    public func scanningStarted() {
        // This gets called every time the scanning cycle loops
        // make sure the UI is up to date
        currentListener?.onRepositoryUpdate()
    }
    
    public func didDiscoverPeripheral(ids: [DiscoverdDevice]) {
        for deviceId in ids {
            insert(deviceUuid: deviceId.uuid, rssi: Int32(truncating: deviceId.rssi), txPower: deviceId.txPower as? Int32, scanDate: Date())
        }
        updateCurrentDevices()
    }
    
}
