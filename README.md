# COMP3931 Individual Project: "2 Meters" app



## Introduction

"2 Meters" is the mobile application that warns users if they get too close to each other (less than 2 meters) by making the user's phone vibrating in 3 different levels: "Caution", "Risk distance" and "Danger!". A distance between 2 users are measured by using the Low Bluetooth Energy signal strength between 2 devices to estimate. After alarming, the detail of the warning is displayed on the app's interface and history for users to keep tracking.

This can be used in public to reduce the spread of coronavirus which helps to reduce the pandemic. 

## Login system

At the start of the app, each user need to log in the system for accessing further functions of the app. For security concerns, the app ensures the user is verify to access their own information and the list of all detected users who was in close contact to the user. It prevents attackers from accessing data to modify or delete without permission. The app ensures to store all user data even after the user logs out. Moreover, the login system helps the app to map links between user data and individual profiles for constructing efficient database which can be used for contact tracing.

User can register an account by entering their email address and an password with some basic information. Once the user input meets the requirements of a standard account, the account will be created and stored in the database.

## Proximity alert

There are 3 levels of alerting:

- Level 1 - "Caution": Warning with a single vibration with the strong boom type. This level alerts when two users start to get close in 2 meters.
- Level 2 - "Risk distance": Warning with 2 times of vibration. This level alerts when two users are in the risk distance to each other which is less than 1.5 meters.
- Level 3 - "Danger!": Warning with 3 times of vibration. This level alerts when two users are in the dangerous distance which is about 0.5 meters. 

After alerting, the detail of the current detection is displayed in the home page of the app and displayed permanently in the history page. Hence, the user can track times and persons they have been in close contact with.
